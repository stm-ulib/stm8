#ifndef ULIB_STM8_S103F_H
#define ULIB_STM8_S103F_H

#include "stm8.h"


// Map GPIO ports
#define PORT_A                        (*(Port*) 0x5000)
#define PORT_B                        (*(Port*) 0x5005)
#define PORT_C                        (*(Port*) 0x500A)
#define PORT_D                        (*(Port*) 0x500F)
#define PORT_E                        (*(Port*) 0x5014)
#define PORT_F                        (*(Port*) 0x5019)

// Map flash
#define FLASH                         (*(Flash*) 0x505A

// Map external interrupt
#define EXT_IT_1                      (*(ExternalInterruptControl1*) 0x50A0)
#define EXT_IT_2                      (*(ExternalInterruptControl2*) 0x50A1)

// Map reset status
#define RESET                         (*(ResetStatus*) 0x50B3)

// Map clock
#define CLOCK                         (*(Clock*) 0x50C0)

// Map watchdog
#define WATCHDOG                      (*(Watchdog*) 0x50D1)

// Map independ watchdog
#define IWATCHDOG                     (*(IndependWatchdog*) 0x50E0)

// Map auto wake up
#define AUTOWAKEUP                    (*(AutoWakeUp*) 0x50F0)

// Map beeper
#define BEEP                          (*(Beep*) 0x50F3)

// Map SPI
#define SPI1                          (*(SPI*) 0x5200)

// Map I2C
#define I2C1                          (*(I2C*) 0x5210)

#endif//ULIB_STM8_S103F_H