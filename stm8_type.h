#ifndef ULIB_STM8_TYPE_H
#define ULIB_STM8_TYPE_H

typedef unsigned char	u08;
typedef signed char		i08;
typedef unsigned short	u16;
typedef signed short	i16;
typedef unsigned long   u32;
typedef signed long     i32;
typedef unsigned char   bool;

#define false           0
#define true            1

#endif//ULIB_STM8_TYPE_H