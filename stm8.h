#ifndef ULIB_STM8_H
#define ULIB_STM8_H

#include "stm8_type.h"

// =@= ASM calls

// Enable interrupts
#define _enableInterrupts()        { __asm__("rim\n"); }

// Disable interrupts
#define _disableInterrupts()       { __asm__("sim\n"); }

// Do nothing
#define _doNothing()               { __asm__("nop\n"); }

// Trap (Soft IT)
#define _trap()                    { __asm__("trap\n"); }

// Wait for interrupt
#define _waitForInterrupt()        { __asm__("wfi\n"); }

// Halt
#define _halt()                    { __asm__("halt\n"); }

// =/= ASM calls

// =@= Shortcuts

// Interrupt
#define interrupt(key, name)    void name(void) __interrupt(key)

// =/= Shortcuts

// =@= Enums

typedef enum {
    FallinEdgeLowLevel   = 0x00,
    RisingEdgeOnly       = 0x01,
    FallingEdgeOnly      = 0x02,
    RisingAndFallingEdge = 0x03
} ExternalInterruptSensetivity;

typedef enum {
    HighSpeedExternal    = 0xB4,
    LowSpeedInternal     = 0xD2,
    HighSpeedInternal    = 0xE1
} ClockMasterType;

typedef enum {
    ClockDivide_1        = 0x00,
    ClockDivide_2        = 0x01,
    ClockDivide_4        = 0x02,
    ClockDivide_8        = 0x03,
    ClockDivide_16       = 0x04,
    ClockDivide_32       = 0x05,
    ClockDivide_64       = 0x06,
    ClockDivide_128      = 0x07
} ClockPrescaler;

typedef enum {
    InternalDivider_1    = 0x00,
    InternalDivider_2    = 0x01,
    InternalDivider_4    = 0x02,
    InternalDivider_8    = 0x03
} ClockInternalDivider;

typedef enum {
    WatchdogPrescaler_4  = 0x00,
    WatchdogPrescaler_8  = 0x01,
    WatchdogPrescaler_16 = 0x02,
    WatchdogPrescaler_32 = 0x03,
    WatchdogPrescaler_64 = 0x04,
    WatchdogPrescaler_128= 0x05,
    WatchdogPrescaler_256= 0x06
} WatchdogPrescaler;

typedef enum {
    IndependWatchdogOff  = 0x00,
    IndependWatchdogEdit = 0x55,
    IndependWatchdogReset= 0xAA,
    IndependWatchdogOn   = 0xCC
} IndependWatchdogKey;

typedef enum {
    TimebaseNoInterrupt  = 0x00,
    TimebaseDiv1         = 0x01,
    TimebaseDiv2         = 0x02,
    TimebaseDiv2p2       = 0x03,
    TimebaseDiv2p3       = 0x04,
    TimebaseDiv2p4       = 0x05,
    TimebaseDiv2p5       = 0x06,
    TimebaseDiv2p6       = 0x07,
    TimebaseDiv2p7       = 0x08,
    TimebaseDiv2p8       = 0x08,
    TimebaseDiv2p9       = 0x0A,
    TimebaseDiv2p10      = 0x0B,
    TimebaseDiv2p11      = 0x0C,
    TimebaseDiv2p12      = 0x0D,
    TimebaseDiv5x2p11    = 0x0E,
    TimebaseDiv302p11    = 0x0F
} AutoWakeUpTimebase;

typedef enum {
    Beep8                = 0x00,
    Beep4                = 0x01,
    Beep2                = 0x02
} BeepType;

typedef enum {
    FirstMSB             = 0x00,
    FirstLSB             = 0x01
} FrameFormat;

// =/= Enums

// =@= Structures

// Allow access single pin at one operation (so possible values only 0/1)
typedef struct {
    u08 Pin0                        :1;
    u08 Pin1                        :1;
    u08 Pin2                        :1;
    u08 Pin3                        :1;
    u08 Pin4                        :1;
    u08 Pin5                        :1;
    u08 Pin6                        :1;
    u08 Pin7                        :1;
} SinglePins;

// Access to all pins (to minimize operations)
typedef union {
    u08 All;
    SinglePins Single;
} Pins;

// Port structure
typedef struct {
    Pins Output;     // aka ODR
    Pins Input;      // aka IDR, Read-only
    Pins Direction;  // aka DDR
    Pins Control1    // aka CR1
    Pins Control2    // aka CR2
} Port;



typedef struct {
    u08 FixedTime                   :1;
    u08 Interrupt                   :1;
    u08 ActiveHaltPowerDown         :1;
    u08 HaltPowerDown               :1;
    u08                             :4;
} FlashControl1;      // aka FLASH_CR1

typedef struct {
    u08 StandardBlockProgramming    :1;
    u08                             :3;
    u08 FastBlockProgramming        :1;
    u08 BlockErase                  :1;
    u08 WordProgramming             :1;
    u08 WriteOptionBytes            :1;
} FlashControl2;      // aka FLASH_CR2

typedef struct {
    u08 NStandardBlockProgramming   :1;
    u08                             :3;
    u08 NFastBlockProgramming       :1;
    u08 NBlockErase                 :1;
    u08 NWordProgramming            :1;
    u08 NWriteOptionBytes           :1;
} FlashNControl2;     // aka FLASH_NCR2

typedef struct {
    u08 UserBootSize                :6; // Read-only
    u08                             :2;
} FlashProtection;

typedef struct {
    u08 UserBootSize                :6; // Read-only
    u08                             :2;
} FlashNProtection;

typedef struct {
    u08 AttemptWriteProtected       :1; // Read-only
    u08 ProgramFlashUnlocked        :1; // Read-only
    u08 EndOfProgramming            :1; // Read-only
    u08 DataFlashUnlocked           :1; // Read-only
    u08                             :2;
    u08 EndOfHighVoltage            :1; // Read-only
    u08                             :1;
} FlashStatus;

typedef struct {
    FlashControl1 Control1;
    FlashControl2 Control2;
    FlashNControl2 NControl2;
    FlashProtection Protection;
    FlashNProtection NProtection;
    FlashStatus Status;
    u16                             :16;
    u08 ProgramFlashKey             :8;
    u08                             :8;
    u08    DataFlashKey             :8;
} Flash;



typedef struct {
    ExternalInterruptSensetivity A  :2;
    ExternalInterruptSensetivity B  :2;
    ExternalInterruptSensetivity C  :2;
    ExternalInterruptSensetivity D  :2;
} ExternalInterruptControl1;

typedef struct {
    ExternalInterruptSensetivity E  :2;
    u08 TopLevelInterruptSensetivity:1;
    u08                             :5;
} ExternalInterruptControl2;


typedef struct {
    u08 WindowWatchdogFlag          :1;
    u08 IndependWatchdogFlag        :1;
    u08 IllegalOpcodeFlag           :1;
    u08 SWIMResetFlag               :1;
    u08 EMCResetFlag                :1;
    u08                             :3;
} ResetStatus;



typedef struct {
    u08 HighSpeedInternalEnable     :1;
    u08 HighSpeedInternalReady      :1;
    u08 FastWakeUp                  :1;
    u08 LowSpeedInternalEnable      :1;
    u08 LowSpeedInternalReady       :1;
    u08 RegulatorPoweroff           :1;
    u08                             :2;
} ClockInternal;

typedef struct {
    u08 HighSpeedExternalEnable     :1;
    u08 HighSpeedExternalReady      :1;
    u08                             :6;
} ClockExternal;

typedef struct {
    u08 SwitchBusy                  :1;
    u08 SwitchStartStop             :1;
    u08 SwitchInterruptEnable       :1;
    u08 SwitchInterruptFlag         :1;
    u08                             :4;
} ClockSwitchControl;

typedef struct {
    ClockPrescaler ClockPrescaler   :3;
    ClockInternalDivider InternalPrescaler :2;
    u08                             :3;
} ClockDivider;

typedef struct {
    u08 I2C                         :1;
    u08 SPI                         :1;
    u08 Serial_24                   :1;
    u08 Serial_13                   :1;
    u08 Timer_46                    :1;
    u08 Timer_25                    :1;
    u08 Timer_3                     :1;
    u08 Timer_1                     :1;
} ClockPeripheral1;

typedef struct {
    u08 Enable                      :1;
    u08 AuxiliaryOSCConnected       :1;
    u08 DetectionInterrupt          :1;
    u08 Detection                   :1;
    u08                             :4;
} ClockSecuritySystem;

typedef struct {
    u08 Enable                      :1;
    u08 Selection                   :4;
    u08 Ready                       :1;
    u08 Busy                        :1;
    u08                             :1;
} ClockOutput;

typedef struct {
    u08                             :2;
    u08 AutoWakeUp                  :1;
    u08 AnalogDigitalConvertor      :1;
    u08                             :3;
    u08 CANBus                      :1;
} ClockPeripheral2;

typedef struct {
    u08 IntenalTrim                 :4;
    u08                             :4;
} ClockTrimming;

typedef struct {
    u08 Divide2                     :1;
    u08                             :7;
} ClockSWIM;

typedef struct {
    ClockInternal Internal;
    ClockExternal External;
    ClockMasterType MasterStatus;
    ClockMasterType MasterSwitch;
    ClockSwitchControl SwitchControl;
    ClockDivider Divider;
    ClockPeripheral1 Peripheral1;
    ClockSecuritySystem Security;
    ClockOutput Output;
    ClockPeripheral2 Peripheral2;
    ClockTrimming Trim;
    ClockSWIM SWIM;
} Clock;



typedef struct {
    u08 Counter                     :7;
    u08 Activate                    :1;
} WatchdogControl;

typedef struct {
    u08 Counter                     :7;
    u08                             :1;
} WatchdogWindow;

typedef struct {
    WatchdogControl Control;
    WatchdogWindow Window;
} Watchdog;



typedef struct {
    IndependWatchdogKey Key         :8;
    WatchdogPrescaler Prescaler     :3;
    u08                             :5;
    u08 Reload                      :8;
} IndependWatchdog;



typedef struct {
    u08 EnableMeasurement           :1;
    u08                             :3;
    u08 EnableAutoWakeUp            :1;
    u08 AutoWakeUpFlag              :1;
    u08                             :2;
} AutoWakeUpControl;

typedef struct {
    AutoWakeUpControl Control;
    u08 Prescaler                   :6;
    u08                             :2;
    AutoWakeUpTimebase Timebase     :4;
    u08                             :4;
} AutoWakeUp;



typedef struct {
    u08 Prescaler                   :5;
    u08 Enable                      :1;
    BeepType Type                   :2;
} Beep;



typedef struct {
    u08 ClockPhase                  :1;
    u08 ClockPolarity               :1;
    u08 MasterSelection             :1;
    u08 BaudRate                    :3;
    u08 Enable                      :1;
    FrameFormat FrameFormat         :1;
} SPIControl1;

typedef struct {
    u08 InternalSlaveSelect         :1;
    u08 SoftwareSlaveManagement     :1;
    u08 ReceiveOnly                 :1;
    u08                             :1;
    u08 CRCNext                     :1;
    u08 CRCEnable                   :1;
    u08 BiDirectionalInOut          :1;
    u08 BiDirectionalEnable         :1;
} SPIControl2;

typedef struct {
    u08                             :4;
    u08 WakeUp                      :1;
    u08 Error                       :1;
    u08 ReceiveNotEmpty             :1;
    u08 TransmitNotEmpty            :1;
} SPIInterrupt;

typedef struct {
    u08 ReceiveNotEmpty             :1;
    u08 TransmitNotEmpty            :1;
    u08                             :1;
    u08 WakeUpFlag                  :1;
    u08 CRCErrorFlag                :1;
    u08 ModeFault                   :1;
    u08 OverrunFlag                 :1;
    u08 Busy                        :1;
} SPIStatus;

typedef struct {
    SPIControl1 Control1;
    SPIControl2 Control2;
    SPIInterrupt Interrupt;
    SPIStatus Status;
    u08 Data                        :8;
    u08 CRCPolynominal              :8;
    u08 CRCReceive                  :8;
    u08 CRCTransmit                 :8;
} SPI;




typedef struct {
    u08 Enable                      :1;
    u08                             :5;
    u08 GeneralCallEnable           :1;
    u08 NoStretch                   :1;
} I2CControl1;

typedef struct {
    u08 Start                       :1;
    u08 Stop                        :1;
    u08 Acknowledge                 :1;
    u08 AcknowledgePosition         :1;
    u08                             :3;
    u08 SoftwareReset               :1;
} I2CControl2;

typedef struct {
    u08 Address10Bit0               :1;
    u08 Address                     :7;
} I2CAddressL;

typedef struct {
    u08                             :1;
    u08 Address10Bit89              :2;
    u08                             :3;
    u08 AddressConfig               :1;
    u08 AddressMode                 :1;
} I2CAddressH;

typedef struct {
    u08 StartBit                    :1;
    u08 AddressSent                 :1;
    u08 ByteTransferFinished        :1;
    u08 Address10Bit0               :1;
    u08 StopFlag                    :1;
    u08                             :1;
    u08 ReceiveNotEmpty             :1;
    u08 TransmitEmpty               :1;
} I2CStatus1;

typedef struct {
    u08 BusError                    :1;
    u08 ArbitrationLost             :1;
    u08 AcknowledgeFailure          :1;
    u08 Overrun                     :1;
    u08                             :1;
    u08 WakeUp                      :1;
    u08                             :2;
} I2CStatus2;

typedef struct {
    u08 MasterMode                  :1;
    u08 BusBusy                     :1;
    u08 Transmitting                :1;
    u08                             :1;
    u08 GeneralCallHeader           :1;
    u08                             :2;
    u08 DualFlag                    :1;
} I2CStatus3;

typedef struct {
    u08 Error                       :1;
    u08 Event                       :1;
    u08 Buffer                      :1;
    u08                             :5;
} I2CInterrupt;

typedef struct {
    u16    Clock                    :12;
    u08                             :2;
    u08 FastModeDuty                :1;
    u08 FastMode                    :1;
} I2CClockControl;

typedef struct {
    u08 Time                        :6;
    u08                             :2;
} I2CRiseControl;

typedef struct {
    I2CControl1 Control1;
    I2CControl2 Control2;
    u08    Frequency                :6;
    u08                             :2;
    I2CAddressL    AddressL;
    I2CAddressH AddressH;
    u08                             :8;
    u08 Data                        :8;
    I2CStatus1 Status1;
    I2CStatus2 Status2;
    I2CStatus3 Status3;
    I2CInterrupt Interrupt;
    I2CClockControl ClockControl;
    I2CRiseControl TimeRise;
    u08                             :8; // Errors
} I2C;

// =/= Structures

// =@= Constants

#define FLASH_PROGRAM_KEY1          0x56
#define FLASH_PROGRAM_KEY2          0xAE
#define FLASH_DATA_KEY1             0xAE
#define FLASH_DATA_KEY2             0x56

// =/= Constants

#endif//ULIB_STM8_H